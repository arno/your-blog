# Add more pages

#### and link them together

Before we can move on, it is time to unclutter our current page.

## Clean up

Your webpage is cluttered, it contains HTML markup and CSS declarations. That might not feel like a big problem - until we start adding content.

Why that is, you ask? Well, if every webpage of your blog contains (copies) of your CSS, making changes to your layout becomes a problem; it is scattered/duplicated all over the place. The result of that is that,
in order to make one fix consistently over all pages, you have to change code in many different places. (Developers have a silly name for this flaw: shotgun surgery)

Therefore: **HTML and CSS should not mix** (in your code, that is)

A visual representation of your code base:

![clean-up](../images/clean-up.jpg)

Let's unmess this mess.

### One file per concept

All stylesheets belong in one directory, nothing else should be there. It is a good idea to have a folder for all things CSS.

In the directory `your-blog`, create a sub-directory called `styles`. In that new folder, create a file called `my-blog.css`

In the HTML page, load that file:

```html
<head>
  <meta charset="utf-8" />
  <title>Your Blog</title>
  <link rel="stylesheet" href="styles/my-blog.css">
  ...
</head>
```

Move all CSS (the part inside the `<style>` tags, **not including those tags**) from your HTML page to your external stylesheet (the `styles/my-blog.css` file, that is)

Reload the page in your browser. Does it still work (& look good)?

### End result after the spring cleaning

Your HTML file knows about the contents of your page, and it knows where to load all needed styles to make it look good.

Your CSS only contains style information, and it doesn't have to be duplicated if you add more webpages (i.e. the goal of this chapter).

If your styles are wrong: You know which (small) file to edit, and update it for your whole blog!

## Step 2: Create extra content

That is what this is all about: Adding content to your blog.

Let's make a webpage about food - what else is there to write about?

In the same folder as your `index.html`, create a file called `food.html`.

The index page has all the not-so interesting parts that we need in our food page as well.

Copy-paste **all** the contents from `index.html` into `food.html`.

Now, for all the boring parts we're done: The `head` of the page is fine. We don't want to change the `footer`, nor the `h1` of the page.

Time to make changes that you expect in a page about food: there should be an article.
Think about a newspaper for a while. What does an article in a newspaper have?

* A heading
* Some paragraphs
* Maybe photos

Let set this up. Below you find the html-setup for an article inside the body of your page. We will put in some space for images as well, although we won't include images yet.
This setup mimics the above behavior (without the actual photos for now).

* The body of the page has a primary heading - like the name of your newspaper. -the `h1` that was already there
* Then there is an article
  * Your article has a heading of its own, a secondary heading
  * Then there are multiple paragraphs with the beef of the page (pun intended)
  * There is a `div` (short for division) in the article that will hold our instagram images. It has an `id` attribute, we will see later why that is.
* Below your article is the footer

```html
<body>
  <h1 class="primary">Your own weblog</h1>

  <article>
    <h2 class="secondary">Your heading on food</h2>
    <p>Some paragraphs describing what your food is</p>
    <p>Maybe you prepare it yourself?</p>
    <p>Or your favorite food is served at a restaurant?</p>
    <div id="instagram-feed"></div>
  </article>

  <footer class="primary">this weblog is built @ Make Sense of Code</footer>
</body>
```

Overwrite the body of `food.html` with the thing above.
Then write some content about your recipe/restaurant/preparations in the article.
Check in your browser to see if the css still works.

PS: Only the body should change, all the info in the `<head>` is needed on both pages.

### Put some HYPER in HTML

Remember that HTML is an abbreviation for HyperText Markup Language? Maybe I've never told you, but now you know it anyway.
Hyper was a very fashionable term from the 1990s, it sounds silly now. But what it means is still important; Hypertext allows you to jump from one page to another.

That is exactly what we need.

In our index page, we should let our visitors know that we have a page on food, right? And (hyper-) link to it.


A link works like this: You build an *anchor* element, abbreviated as `<a>`. The anchor has an attribute where the visitor should go if s/he clicks the link. That is called the  *hyper reference*, abbreviated as `href`. Then between the open tag and the close tag of that anchor is the text that the visitor sees on your page (no surprise here I hope, the concept of elements contents has so landed in your brain already since hours ago).

Open index.html in your editor, and add an `a` element that has a reference to (or links to) `food.html`

Look for the line that reads

```html
<p>See my articles</p>
```

Below that line you want to link to your food article:

```html
<p>See my articles</p>
<p><a href="food.html"> on food </a></p>
```

### Put **more** hyper in your html

Question: What happens on any website if you click on the big heading at the top of the page?

Right: It brings you to their homepage. In our weblog, we expect the same behavior.

Open `food.html` in your editor. Convert your primary heading into a primary heading that links to your index page:

What it is:

```html
<h1 class="primary">>My own weblog</h1>
```

What it should become:

```html
<h1 class="primary"><a href="index.html">My own weblog</a></h1>
```

Fantastic! Your index page (the homepage in normal-people speak) links to your food page, and vice versa!

---

## Exercise

*The goals of this exercise*:

* You split your CSS and HTML in 2 different files, without breaking the way it looks in your browser
* You add a page to your weblog, and you can navigate from the index to your content page and back by clicking on links
* Your content page has an element that will hold your instagram feed.

### Do it yourself

* Make a `styles` directory and put your CSS in a file called `my-blog.css`
* Load that new file from your index.html
* Create a page for your favorite dish
* Link your pages using an `a` element
* In your content page, create a `div` element that has an id attribute with the value `"instagram-feed"`. (`<div id="instagram-feed">This is a placeholder text; my images go here</div>`)
