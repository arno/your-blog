# Using Javascript

#### and its basic building blocks

There are 2 basic building blocks, the number and the string.

## Numbers

Numbers are fantastic for calculations

* `8` (just a number) returns `8`
* `4 * 6` (multiplication) returns `24`
* `4 / 6` (division) returns `0.666666666666`

## Strings

Strings are fantastic for communicating with other humans

* `"I am almost 42"` returns `"I am almost 42"`
* `'4 / 6'`(a string with some numbers) returns `"4 / 6"`

So, numbers can be used in calculations, and strings can be used to do textual ~~strings~~ things.

If only we had a nice way of keeping track of all the numbers and strings we need in our application.

## Variables

Variables are fantastic for keeping values stored

As you saw, `"Hello, World!"` returns `"Hello, World!"`

If something is returned by javascript, you can store it in a variable. Let's say we want have this text stored in a container with the name `greeting`

`greeting = "Hello, World!"` stores that string on the right hand side of the equal sign in a variable with the name `greeting`

```js
greeting = "Hello, World!"

greeting // => returns "Hello, World!"
```

So, a variable is like a small container that has a name, and it holds some value.

You can store any value in a variable

> You give them a name (like age)
> and store a value (like 42)
> `age = 42`

## What? Wait? What?

Lets *combine strings, numbers and variables* to see if it starts to make sense...

```js
age = 42
introduction = "I am almost " + age
// now the variable introduction stores the value "I am almost 42"
```

Another way of writing this is:

```js
age = 42
introduction = `I am almost ${ age }`
```

The 2 things above look a bit different, but they do the same thing.

There are three different strings:

* `"` - the double quote
* `'` - the single quote
* <code>`</code> - the backtick

```js
myFavoriteColor = "purple"

"My T-shirt is ${ myFavoriteColor }" // returns "My T-shirt is ${ myFavoriteColor }"
'My T-shirt is ${ myFavoriteColor }' // returns "My T-shirt is ${ myFavoriteColor }"
`My T-shirt is ${ myFavoriteColor }` // returns "My T-shirt is purple"
```

Lets update that list from above:

* `"` - the double quote
* `'` - the single quote
* <code>`</code> - the backtick → 🤓 The most magic! 🐇+🎩

---

## Exercise (in the console)

The goal of this exercise is that you play with the basic building blocks (`string` and `number`), and experiment with variables.

Make sure to use the proper quote marks.

* The `'` and `"` quotes are on the right of your keyboard,
* the back tick (<code>\`</code>) is located left of the number `1` (Windows, maybe Apple), or left from the letter `Z` (some other Apples)

Now for the exercise:

* calculate `9 * 2` and store the result in a variable called `almostTwenty`.
* See what happens when you type `almostTwenty` and hit enter.
* Use that number, what happens when you enter:

```js
`If only I remember the time I was ${ almostTwenty }`
```
