# CSS selectors

#### styling with class

While designing and picking colors, you will probably run into 2 (sometimes  3) colors that could be the base of your color scheme.

It makes sense to style all your important elements in your primary color, and your somewhat important bits in your secondary color.

Or, in human speak, a certain class of elements play a primary role, another class of elements play a secondary role, and the rest is just not special at all.

CSS and HTML think in the same way. HTML has attributes; you saw that in Chapter 1. Time to put attributes to work.

You can make HTML elements and set their `class` attribute to the value `"primary"`:

```html
<h1 class="primary">Your own Header</h1>
...
<footer class="primary">Your own footer</footer>
```

That on its own doesn't change a thing; your browser does not read your mind (yet). Lets see how we can apply styles to that class.

If you want to style the elements with the class set to `primary`, you can select them by prefixing the class name with a dot in your css selector: `.primary`.

```css
.primary {
  color: #805;
}
```

To recap: The above `.primary` selector will match **all** HTML elements that have the `class`-attribute set to `'primary'`.

The color of the above primary class is set to the value `#805`, which is a geek way of writing purple. Actually, it is called RGB, not geek way. Google for it if you want to know more (you know you do!).

---

## Exercise

Goal of the exercise: You can change the styles of multiple elements by
changing the style of their class. Furthermore, you should get more
confident in changing HTML.

* Apply the `primary` class to the `h1` element
* Apply the `secondary` class to the `h2` and the `footer` element
* Create styles and to colorize the `primary` and `secondary` HTML classes, according to your taste.

My end result looked like this (which is a nice warning how not to use colors and/or fonts in your design):

![chapter-4-end-result](../images/chapter-4-end-result.png)
