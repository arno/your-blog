title = document.getElementById('title')
title.innerText = articleTitle
// console.log(title, articleTitle)

instagramFeed = document.getElementById('instagram-feed')

baseLocation = "https://instagram.com/p/"
mediaSuffix = "/media/?size=m"

instagramShortLinks.forEach(shortLink => {

  image = document.createElement('img')
  image.src = baseLocation + shortLink + mediaSuffix

  link = document.createElement('a')
  link.href = baseLocation + shortLink
  link.appendChild(image)

  instagramFeed.appendChild(link)
});
