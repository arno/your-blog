# 02  Intro to CSS

#### Present your content in style

CSS is as stylesheet language. It defines what your pages look like.

It can change the style of your elements:

* your text (font and color)
* the elements themselves (background-color and spacing)
* background images and colors for your whole page

## CSS is the only language for styling your webpages

Without any styling applied, a webpage could look like this:

![html-no-style](../images/html-what-is-it.png)

Using a fantastic style declaration, the page could suddenly looks so much better...
![html-with-style](../images/html-what-is-it-with-style.png)

🤔 _...Well... potentially... maybe... it might look better_ 🤔

### All nice and dandy, but how does that work?

```html
<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8" />
  <title>Your Blog</title>
  <style>
    h1 {
      color: red;
    }
  </style>
</head>

<body>
  <h1>Your own weblog</h1>
  ...
</body>
</html>
```

The thing to note here is the additional element in the head of the page: `<style>`.

In that element you can declare the style of your page. That is why it is in the head of the page, and not the body: It is metadata describing your page, it is not the actual content of the page.

## Dissecting CSS rules

```css
h1 {
  color: red;
}
```

| word | meaning or definition |
| - | - |
| `h1` | The **selector**; selecting the part(s) of the HTML that the style applies to
| `color` | The **property** name; the thing we want to style
| `red` | The property **value**; what we want to style it after?

Thing to note: Everything between the curly braces `{` and `}` is called the **CSS declaration**.

Just some 🤓 geeky definitions, now let's get to it and use this knowledge!

---

## Exercise

The goal of this exercise is to make sure you can write CSS, on the right place in your HTML file. And that it works!

Make that page light blue.

Think about which element of the HTML represents the whole page. Is that `p`? `h1`? or maybe the `body` or `html`, or `head`?

In case you run out of ideas, ask your neighbor, as a last resort try googling for 'css change background color html page'

![chapter-2-end-result](../images/chapter-2-end-result.png)
