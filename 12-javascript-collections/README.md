# Javascript Collections
*super-variables that store lots of things*

I don't know how that felt for you, I think it was cheating :/

You're here learning coding for the web, and you copy-paste many lines to see some images. We're geeks now. We can do better.

If an article has many images, we don't want to code the repeating parts

The article can have 3 different images:

```terminal
https://instagram.com/p/BnEQoOkH5HV/media/?size=m
https://instagram.com/p/BnEQoOkgAcV/media/?size=m
https://instagram.com/p/BnEWOk100cZ/media/?size=m
```

That feels like loads of duplication, while the actually interesting parts are only the shortLinks:

```js
BnEQoOkH5HV
BnEQoOkgAcV
BnEWOk100cZ
```

If only we could store a collection of these interesting parts.

## Meet our new best friend: The `Array`

```js
instagramShortLinks = [
  "BnEQoOkH5HV",
  "BnEQoOkgAcV",
  "BnEWOk100cZ",
]
```

🤔 What does that above thingy even mean?

An array is a collection of things,
between block parenthesis [ ],
and every element is separated
by a comma.

---

## Exercise

The goal of this exercise is to start exploring arrays, and logging them to the console (without breaking a sweat)

In your `food-data.js`, create a variable (called `instagramShortLinks`) that holds an array with **only** the shortLinks of the images you want for your blog page.

Underneath that line, log that variable to your console.

Open your browser. Does it show up in your console? Yes? Then you're done. Get a glass of water!
