# Web Fonts

#### Always good looking

The look of a webpage is important. Maybe you want to style your content using a specific font.
If you use fonts from the web, you can be as creative as needed because it will look as fantastic on their screen as it does on yours.

Regarding those Fonts: Google has an extensive library of fonts that are free to use in your webpage (either commercially or for your pet project)

Visit <a href="https://fonts.google.com/">fonts.google.com</a> to scroll through their collection of fonts.

![google-fonts](../images/google-fonts.png)

If you see a nice specimen, click on the red (+) icon, and keep on browsing till you're satisfied.

![red-plus-drawer](../images/red-plus-drawer.png)

Open the slide at the bottom of the screen for instructions.

![drawer-opened](../images/font-family-drawer-open.png)

The page you see informs you on 2 steps you have to take:

1. Make a link to a stylesheet from the head of the page.
1. Use the CSS declaration provided, to use the font on certain elements of your blog

In short: Nothing to download, no files to put in your project. After making the above changes,
everything is done by the browser.

## What does that look like?

Step 1, make a link to the stylesheet from Google:

```html
<head>
  ...
  <link href="https://fonts.googleapis.com/css?family=Poppins|Lato" rel="stylesheet">
</head>
```

Then step 2, we have to use our fonts, by setting css declarations. I'd like my
headings to be in Poppins, and all the other characters to be in Lato.

There is a nice css trick to accomplish that. By styling the `body` selector, not only the body, but all descendent elements will inherit those styles, except when they are explicitly overridden.

```css
body {
  background-color: lightblue;
  font-family: 'Lato', sans-serif;
}

h1 {
  font-family: 'Poppins', sans-serif;
}

h2, footer {
  font-family: 'Poppins', sans-serif;
}
```

As a free tip, put each property and value combination on its own line, hitting Enter after the semi-colon (e.g. the ';' sign). That way, your styling looks pretty when reading it.`

---

## Exercise

The goals of this exercise are:

* to play with css selectors
* apply a style to multiple elements in one go

Use external fonts to spice up your page!

1. Make your headings stand out
2. Make the body of your page pretty without sacrificing readability
