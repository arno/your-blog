# Using Javascript

#### in your webpage

Lets tie Javascript and HTML together

Just as before, you need a directory for all the Javascript files. Lets call it `scripts`.
Add a file for our articles. In your scripts directory, create a file called `article.js`.
Then you should load the (empty) script at the end of the `head` of your HTML.

```html
  ...
  <script defer src="scripts/article.js"></script>
</head>
```

Maybe this `<script>` tag can be self closing, maybe it cannot be. Test it if you want to experiment!

This loads the `article.js` file in your webpage, so it can start interacting with your page.

Do that in both your index page, and your food page.

Now, add some code to that `script/article.js`-file.

```js
// You will add lots of Javascript here - not now, later
console.log('It works!')
```

All the time you've been playing with the console, but now you log a message from the Javascript file into that console.

---

## Exercise

**The goal of this exercise** is to make sure your files reference each other correctly. Also it should boost your confidence if you see your message appear in the console if you reload the page.

* Create a folder for your scripts
* Create a file for your article related code: `article.js`
* In that file, log a message to the console
* In your browser: Check that it works
