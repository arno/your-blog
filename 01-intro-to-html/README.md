# 01: Intro to HTML

#### Structure for your content

## Some definitions

* HTMLis a markup language used for writing web pages
* Humans and computers can read it
* It is the only building block  for any webpage you ever visited *

\* = *Regarding content that is...*

Lets look at an example:

```html
<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8" />
  <title>Your Blog</title>
</head>

<body>
  <h1>Your own weblog</h1>

  <h2>Tasty food</h2>
  <p>How to make it and what it looks like</p>

  <footer>This weblog is built @ Make Sense of Code</footer>
</body>

</html>
```

| element | description |
| --- | --- |
| DOCTYPE | Not interesting for humans, important for browsers|
| head | Metadata for the page - not the page itself |
| meta | A special tag, informing your browser that we are possibly using international characters|
| title | The title of the page, as displayed in your browser tab |
| body | The content that your user sees |
| h1 - h6 | different size of headings (think Word) |
| p | A paragraph |
| footer | An elements that wraps up your contents |


Note: There are many <sup>many <sup>many <sup>many</sup> </sup></sup> more elements than the above list!

## What is an element?

HTML elements open and close

```html
            element
            contents

<title>     Your Blog     </title>

opening                   closing
tag                       tag
```

### HTML tags can have attributes

Consider the h1 element below. In the opening tag, the  `lang` attribute is set to the value `nl`, to inform the browser that the contents are in Dutch (which is clearly wrong, by the way)

```html
<h1 lang="nl"> Your own weblog </h1>

lang: attribute name
nl: the corresponding value
```

### Some HTML tags are self-closing.

Some elements do not have textual element contents. Elements that can have no meaningful contents are self closing.
An image does not have text - the whole point is that you look at the image.

```html
<img src="images/html-what-is-it.png" />
```

If you use this kind of element in your webpage, your browser will display an image (`img` is just short for image) that is found at the source location (or `src` for short).

Time to create a webpage. To get set up, you'll need to make a few steps...

* On your desktop create a folder for your project, named `your-blog`. **Please, no spaces**.
* Open Visual Studio Code and add that folder to your workspace.

![add-folder-to-workspace](../images/add-folder-to-workspace.png)

* Navigate to your newly created folder and add it:

![add-your-blog-to-workspace](../images/add-your-blog-to-workspace.png)

* Close the welcome page that is open by default.
* Your workspace setup should look like this now:

![workspace-setup](../images/workspace-setup.png)

* Create a file in your new folder</p>

![create-a-file](../images/create-a-file.png)

* Name the file <code>index.html</code></p>

![empty-index](../images/empty-index.png)

* Now, you want to display that page in Chrome. Navigate with your file explorer, to your new `index.html`-file, and (right-click to) open it in Chrome.

---

## Exercise

The goal of this exercise is to make sure you have your editor set up correctly,
that you can change a file in it, and that changes will be reflected if you
reload your page in the browser.

Using all knowledge you have about HTML, fill that index page with the proper
elements (`<DOCTYPE>`, `<html>`, `<head>`, `<title>`, `<body>`, et cetera should
all be present).

Open the page in Google Chrome. How, you ask? Well, you created a folder in your desktop, in that folder you created the file. Navigate there with your Finder, Explorer, or whatever you call it. (Right- or double-) click on that thing, possibly click 'open with...' and select Chrome, and see it in all its glory in Chrome.

Make sure that if you open it in Google Chrome, that it looks like this:

![chapter-1-end-result](../images/chapter-1-end-result.png)

Note: The elements that are used in the body of the page are:

* one `h1` a gigantic heading
* one `h2` a big heading
* two `p` (short for paragraph)
* one `footer` for the 'this weblog is built yadayadayada.

Feel free to write different contents if you don't want to write about food, just use to the elements listed above. This skeleton will be expanded upon during this workshop.
