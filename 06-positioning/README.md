# Position your elements

#### Put some air in your design

## Picture elements (pixels, that is)

Every screen is made of pixels. Sometimes it aren't that many pixels (a projector for instance), sometimes it are very many (think a high definition screen).
When positioning elements, it is a good idea to keep that difference in mind; you will be designing your weblog for mobile phones, laptops and possibly projectors.

So how many pixels does an old I-thingy have?

<img src="../images/i-device-pixels.png" style="width:200px;" />

That screen is 320px (or pixels) wide, and 480 px high.

In that image (that is actually 243 px by 481 px), each and every pixel has a certain color. If you zoom in on that image, the individual pixels start to show, or in other words: The 'blow-up' gets blurry.

![fat-blown-pixels](../images/fat-blown-pixels.png)

There is no CSI: Miami 'enhance that image'-engineer to make that look better.

Long story short:

* Each screen has different dimensions (unpredictable) 😕
* Zooming (some) content doesn't look well 😕
* Pixels are the basic building block of displays

Long story even **shorter**: You have to work with it anyway 👍

## What does it have to do with my blog anyway?

Elements have their own width and height that you'd might want to influence.

Let's have a look at an element from a random webpage:

<img src="../images/280x410-upload.png" style="max-height:350px">

To make your elements look good, you sometimes want to set height and width.

Apart from the height and width of the element itself, there are 3 'layers' around the element contents:

<img src="../images/layers of an element.png" style="height:350px">

In the image above (which is not in the correct aspect ratio), we see:

* In the middle an element of 120px by 150 px.
* Around the element: the `padding`, it is 10px on all sides
* Around the padding: the `border`, a line around your element - of 5px in this example)
* Around your border: the `margin`, it is 20px on all sides

The border is something the user sees: it is the line you see around an element - very visible like a frame around a picture.

You could think of the border as a box around your contents. Like an actual card-board box. To move things from one house to the next. Your content is encapsulated by that box.

The padding and the margin are more subtle; they are the breathing space of your content.

The padding is inside that box, it has the same background color as your element, since it sits inside. Like a cushion between the important contents, and the box itself.

The margin is outside the box, it doesn't have the background color of the element, because it is the space between one cardboard box and the next. It is the air around your element, making sure other elements cannot get closer to it. Like a cushion on the outside of your box.

So, if you want to give some space to your design, padding and margin are your friends (and if you know these concepts from Word, it already makes sense. Otherwise, hang in there...)

## Margin, padding and positioning

Let's say you're building a page with that element repeated multiple times, this is how the blocks could be placed next to / underneath each other:

<img src="../images/stacked-elements.png" style="height:350px">

By setting the width and height of elements, influencing their margin and padding, a pretty design can be created.

Maybe you want to display photos to the left, the article of your blog to the right, all wrapped in an element with some background color, and a footer that is centered (and at the bottom of the page)?

It isn't impossible to build a webpage that has some elements set up like this layout:

<img src="../images/toc-your-blog-sketch.jpg" style="height:350px">

Only your imagination will stop you :)

### Positioning

Positioning is the art of laying out your elements in such a way that it is pretty, and not confusing.

The positioning we see most (sometimes referred to as 'normal flow') is just one block of contents after the other.

#### Static positioning

Usually, content flows, one paragraph after another.

Just like the lines you're reading here.

Or as CSS declaration:

```css
body {
  position: static;
}
```

This is how articles in a newspaper read, in Word documents work. Nice for reading large pieces of text, somewhat icky for implementing the design above.

There is also another way of laying out some of your elements: Fixing them to a certain location of the screen.

#### Fixed positioning

Positioning so you can nail it to the screen.

Once you remove an element from static positioning, you do 2 things: You set the `position` property to `fixed`, and then you describe where it should be fixed to the screen. Maybe you want a menu on the top of the page? Set it's `top` property to '0px'. This will move the element 0 pixels away from the top of the screen.

Additionally you'd might want to set the height and width of that element as well, for better control.

An element that is removed from static positioning tends to cover other elements. It can sit on top of text your readers might have an interest in. If that is the case, padding and margin of the elements that would otherwise be covered could save you from that issue.

<img src="../images/fixed-positioning.png" style="max-width:400px;">

We could write the CSS for the above layout:

```CSS
footer {
  position: fixed;
  bottom:   20px;
}

.profile {
  position: fixed;
  right:    10px;
  width:    30%;
}
```

In the above CSS, elements with the class `profile` will be fixed to the right hand side of the screen, and have a width of 30% of that screen.

Now consider this HTML:

```html
<body>
  <article>
    <p>
      An article with very many lines of important text, et cetera - et cetera. An
      article with very many lines of important text, et cetera - et cetera. An article with
      very many lines of important text, et cetera - et cetera. An article with very
      many lines of important text, et cetera - et cetera.
    </p>
  </article>
  <div class='profile'>
    An image and a name:
    <br>
    <img src="..." />
    <br>
    A name
  </div>
</body>
```

The `article` element would get covered by the `div` with the class `profile`. Let's update the styles so that doesn't happen:

```css
.profile {
  position: fixed;
  right:    10px;
  width:    30%;
}
article {
  margin-right: 30%;
}
```

To make sure that the article isn't covered by our profile, we give the article a margin on the right, so it will never use the space. If the article is really long, we can even scroll those contents, without moving the profile.

The same works on the left, top and bottom.

Now, in our page we don't have a `profile`, this was merely meant as exemplary code. Now, of you go to the exercise

---

## Exercise

**Goal of the exercise**: Be confident with your styling. Maybe it wont look amazing at once, but slowly you get to a design that is to your heart's content.

### Write some CSS to layout your page

**🤔 Note:** In the previous chapter, you added a `div` element with an `id` of `instagram-feed`. If you want to style an element with an id, you can select it like below:

```css
/* to match an element with the id 'instagram-feed, use the next line */
#instagram-feed {
  /* your style declarations for this specific element go here */
}
```

* Make your page beautiful, by moving some elements to fixed places of the screen.
* Use google to come up with nice examples, and ~~steal~~ borrow some good ideas.
* hints:
  * [background-color](https://developer.mozilla.org/en-US/docs/Web/CSS/background-color)
  * [border-radius](https://developer.mozilla.org/en-US/docs/Web/CSS/border-radius)
  * [box-shadow](https://developer.mozilla.org/en-US/docs/Web/CSS/box-shadow)
  * [padding-top](https://developer.mozilla.org/en-US/docs/Web/CSS/padding-top)
  * [text-align](https://developer.mozilla.org/en-US/docs/Web/CSS/text-align)
  * [right](https://developer.mozilla.org/en-US/docs/Web/CSS/right)
  * [left](https://developer.mozilla.org/en-US/docs/Web/CSS/left)
  * [bottom](https://developer.mozilla.org/en-US/docs/Web/CSS/bottom)

If you have no inspiration, feel free to mimic the sketch from the example

<img src="../images/toc-your-blog-sketch.jpg" style="height:350px">

All elements from the above picture:

* A primary heading at the top of the page (green outline)
* Then an article, that contains your heading, full width (yellow outline)
  * An article heading aligned to the left (orange outline)
  * Many paragraphs (grey outline)
  * The instagram feed, fixed to the right hand side of the screen (blue outline)
* A footer, fixed at the bottom of the page

All colors are just used to easily identify elements, not because they are so pretty in a webpage 🤣
