# Javascript: basics

#### Laying the foundation for interaction

You'd wonder: What is Javascript? Well, it  is a programming language that runs in the browser.

As such:

* It can make changes to the page it is written for
* It is both easy and hard to learn (the easy part you will do today, the hard part is taking your teacher 10 years already ;) )
* It is the only programming language available to the web browser

Before we start interacting with the page, it is time for the first step: Fiddling with the browser console

## Open your Javascript console

Since you're using Chrome, the key combo's are:

* `CTRL` + `Shift` + `J` (on Windows/Linux)
* `CMD` + `ALT` + `J` (on Mac)
* Or simply hit `F12`, and click on `Console`

<img src="../images/open-your-console.png" style="max-width: 800px;">

---

## Exercise

**The goal of this exercise** is that you know where to find your Javascript console. Then you run some small pieces of Javascript, so you can be surprised how it works.

Play around with your console

Calculate `8 * 5` and hit enter. What do you see?
Type `"Hello, World!"` (including the quotation marks) and hit enter. What do you see?
