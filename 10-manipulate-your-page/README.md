# Manipulate your page with Javascript

If you load a javascript file from your HTML document, that Javascript has access to your whole webpage. That is handy, because it means you can can interact with the page.

## Access the webpage from Javascript

The current webpage is accessible from your Javascript. You can access it from your console. Since webpage sounds too unofficial, it is called `document`.

Open your console and type `document`

<img src="../images/just-document.png" style="max-width: 600px">

It returns something that you can click on to open.

<img src="../images/document-somewhat-open.png" style="max-width: 600px">

You can start inspecting the whole HTML of your webpage from Javascript.

<img src="../images/document-body-visible.png" style="max-width: 600px">

That might not feel all too handy, but it is rather nice. Once you hover over an element in your console, the corresponding element in the page is highligted as well.

<img src="../images/document-with-highlighting.png" style="max-width: 600px">

That is quite nice if your page doesn't do what you think it should.
Also; it does not show you the HTML as you typed it, it shows you the HTML as your browser understands it. By that it is a very fine tool for learning what is going on. Try it. Click on elements, play around with it.

Well.. We can't ask our users to start fiddling with the console, now can we?

### Pinpoint an element from Javascript - Part 1: HTML

HTML elements can have attributes. You know that by heart now. The `id` (short for `id`entifier) is exactly what you need; an attribute to uniquely pick an element.

Let's add an `id` to the `h2` element of the index page, so we can pinpoint it with Javascript.

Convert

```html
<h2 class="secondary">This is the title of my article</h2>
```

into

```html
<h2 class="secondary" id="title">This is the title of my article</h2>
```

The only thing we change is the addition of the `id` with the value of `"title"`.

### Pinpoint an element from Javascript - Part 2: Javascript

The `document` object in Javascript not only shows you what it thinks your elements are, it also has ways of interacting with the page.

So you want to get an element by its id? The `document` has a function that does just that.

```js
document.getElementById('title')
// or slightly better: get the element, and store it in a variable.
title = document.getElementById('title')
```

See it in action:

<img src="../images/getElementById.png" style="max-width: 600px">

**Short recap**
> You can access your webpage with `document`
>
> In your document, you can access elements with
`getElementById('the-id')`

🤔 I wonder if I can change their contents... 🤔

#### Updating an element

As you remember, an HTML element has contents between tags. You could nest other elements in an element, but for our case we just want to set the inner text.

We want to change the text that is inside the opening and closing tags. Or in Javascript terminology: the `innerText`

If we found an element with Javascript, we can change the inner text of that element, by assigning it a new value.

Lets assume this simple html:

```html
<h2 id="title">Hello</h2>
```

If we run the Javascript that is below, we select that element by its id, and then change the inner text from `Hello` into `My new contents`.

```js
articleTitle = "My new contents"
title = document.getElementById('title')
title.innerText = articleTitle
```

If you have this code somewhere in a Javascript file that gets run by the browser if you load the page, it happens so blazing fast that the user never sees the original contents.

Although it usually happens really fast, it is a good practice to have an element without any contents (i.e.  nothing between the opening- and closing tag) instead of contents, so we have no risk of showing wrong content anyway.

### The different parts

1. Your elements.

    There is one element with the `id` of `'title'`. It doesn't have to have any contents of itself.
1. Your text for the title element

    The variable holding the title text for your index page is defined in a file called `index-data.js`. The variable has the name `articleTitle`.

1. The script with interaction

    The interaction depends on these 2 parts, an element with the id `title`, and a variable called `articleTitle`, that has the contents for that element. It will get the element by its id, and change the inner text to whatever the variable holds.

In short:

* There is an `index.html` page with all its static contents
* There is an `index-data.js` file with *only* data for the index.html page
* There is an `article.js` script that dutifully connects the Javascript variables to the HTML page

![image representing index, data and interaction](../images/three-files.png)

Now, if you want to use this setup for the food page, the `article.js` file (with its generic interaction code) doesn't need to change.
On the other hand, the `food.html` page needs to load a javascript file called `food-data.js`, that has a variable called `articleText`, that has the inner text for the title element.

Let's look at a graphical representation

![image representing index, food, and corresponding data and interaction](../images/five-files.png)

---

## Exercise

The goal of this exercise is that pinpoint an element and change it from Javascript. Later today we need this extensively, so make sure you understand what is going on.

There are **lots of** steps to this exercise. Start by reading em all so you know where you're going:

1. Create 2 empty javascript files (in the correct folder), `index-data.js` and `food-data.js`.
1. Load  `index-data.js` from the `head` of your `index.html` file, like so:
    > `<script src="scripts/index-data.js"></script>`
1. Do the same for food, make sure it loads `food-data.js`
1. In `index-data.js`, set the variable `articleTitle` to what you want the page to display. In my case: `articleTitle = "On Tasty food"`
1. Do the same for food, make sure to use the same variable name `articleTitle`.
1. In both `index.html` and `food.html`, clear the `<h2>` of its contents and give it an `id` attribute, with a value of `title`
1. in the file `article.js` write logic to get the element with the `id` of `"title"`, and set its innerText to whatever is stored in `articleTitle`.
