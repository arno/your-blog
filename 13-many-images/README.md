# Display many images

*Make your array work*

It is time to create the images from our collection of shortLinks

First, let's remove our cheat. We cannot have the images hard-coded in our instagram-feed element. Remove that nonsense.

So, how do we go from

```js
instagramShortLinks = [
  "BnEQoOkH5HV",
  "BnEOECSnfiH"
]
```

to

```html
<img src="https://instagram.com/p/BnEQoOkH5HV/media/?size=m" />
<img src="https://instagram.com/p/BnEOECSnfiH/media/?size=m" />
```

???

The answer is: With code, luck and a bit of black magic :)

## Step by step

1. Write javascript (the magic part) to create `img` elements
1. Append all `img` elements to the `instagram-feed`

### Think about the magic

* We have a collection of shortLinks.
  * For each shortLink in the collection, we should
    * create an image element
      * set the src to that base url,
      * add the shortLink in
      * add the media part in
    * and add that image element to the instagram-feed element

It looks a bit silly if I write it like this, but it enables us to thing from very general things (the left hand), to very specific things

### Let's code the magic

```js
// we get the instagram-feed element, that we will later append the images to
instagramFeed = document.getElementById('instagram-feed')
// Now, for each shortLink in the collection of shortlinks, we want to do something.
//
// It is a bit hard to read, but the code between the { and } characters is run for each shortLink.
amountOfShortLinks = instagramShortLinks.length
for(number = 0; number < amountOfShortLinks ; number = number +1) {
  // access the shortLink
  shortLink = instagramShortLinks[number]

  // make a string that points to the image on Instagram
  imageLocation = `https://instagram.com/p/${shortLink}/media/?size=m`

  // the document is told to create an <img> element
  image = document.createElement('img')

  // the src attribute of that image is set our imageLocation
  image.src = imageLocation

  // and the image is appended to the screen
  instagramFeed.appendChild(image)

  // and we're done
}
```

---

## Exercise

The goal of this exercise it to get a feel for the power of programming.

Run the code above in your console. What does it do?

Add the code to `article.js`. Refresh your page. Are your images still there?

Check your console if there are errors.

If your images do not show, make sure that all variables are spelt correctly: Javascript is case sensitive (ie `Title` is something else than `title`). Check the spelling of all your variables.
