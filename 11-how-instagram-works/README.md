# How Instagram works

*Regarding linking to pictures, that is*

Sooo, at last we see images :)

###  The stream (cold)

Instagram has a stream. If you go to instagram.com, you immediately see it for your account. Although it is quite handy that Instagram has this, it is useless to us.

<img src="../images/instagram-01-stream.png" style="max-width=600px">

### A user's collection (warmer)

A user has a collection of images. If you go to your own account, you see you own images.

That is handy, now you know for sure you're only looking at images you can safely use, no copyright restrictions apply :)
<img src="../images/instagram-02-image-overview.png" style="max-width=600px">

Let's click on one of these images

### The detail view of a picture (hot)

Let's say we want to use this image. We're almost there. We aren't downloading the image, no, we will be smarter, we're linking to the images on instagram, just as we didn't download the fonts (when the day was still young).
But we're in the right place. As long as you know what to look for...

<img src="../images/instagram-03-image-detail.png" style="max-width=600px">

And the place we should look for is in the address bar

### The shortLink in the address! (🔥🔥🔥)

In the URL as a cryptic looking thing, which actually is a (unique) identifier for the image you're looking at.

<img src="../images/instagram-04-image-detail-id-highlighted.png" style="max-width=600px">

---

### Once you know the shortLink, displaying images is 🥜🥜

I've been digging through the documentation of Instagram (so you don't have to).

All pictures can be found in a very simple way. There is a

| | |
| --- | --- |
| base path | `https://instagram.com/p/` |
| the shortLink | `BnEQoOkH5HV` |
| and the media suffix | `/media/?size=m` |

If you glue them together, you get the URL to your picture!
`https://instagram.com/p/BnEQoOkH5HV/media/?size=m`

Now, we could make an anchor to that thing, but that means we don't display the images on our blog. As an extra issue, the user is off to Instagram, so they won't return for another 30 minutes...

Instead, we will make an image element based on that URL:
```html
<img src="https://instagram.com/p/BnEQoOkH5HV/media/?size=m" />
```

In case you're wondering what that looks like, here is the exact same thing, as your browser would see it:
![](https://instagram.com/p/BnEQoOkH5HV/media/?size=m)

---

## Exercise

**The goal of this exercise** is to (finally) display some images in your blog

Spice up your food page!

* Find your images about food
* Get hold of their shortLinks
* Create `<img>` elements and display the images in your `div` with the `id` of `"instagram-feed"`
