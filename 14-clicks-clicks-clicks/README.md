# Clicks, clicks, clicks (€/$/¥)
*Drive visitors to your instagram page*

We have images on our screen. You'd might be tempted to look better at them. Too bad you can't click on them and see a better version.

Wait... We are building this thing. Lets make our images clickable!

## Linking images

What does your user expect when clicking on an image?
> possibly a better view of the image?

That sounds like a lot of work. We have to make some styles, download bigger images... If only we didn't have to write all that code...

Well, all developers agree: laziness always pays of now. We should be as lazy as possible: not building something is smarter than building it. And...  Instagram is better at solving this problem anyway.

We should link our images back to Instagram, and the whole problem is solved.

That sounds harder than it actually is. Let me ask you: What is an image that links somewhere?

Well, a link to `somewhere`, and as contents for that link element, we can use an `img` element.

```html
<a href="/somewhere"><img src='picture.jpg' /></a>
```

Hard? Nah, it is solved!

### Where should we link to?

The location of our media and the place on the internet that displays the full page have almost an identical URL:

Our media (image source) is available here:

`https://instagram.com/p/${shortLink}/media/?size=m`

And the page that displays thay image with all things Instagram is at this location:

`https://instagram.com/p/${shortLink}`

That is the same thing, minus the media-suffix :)

## Time to start coding

How to update your Javascript?

Our basic building blocks:

⁍ the URL for the page:

"https://instagram.com/p/" + shortLink

⁍ the image source:

"https://instagram.com/p/" + shortLink + "/media/?size=m"

### 🤔 developers hate repetition 🤔

The benefit of not duplicating code is that it is easier to change if we want to do that. Think about all failing IT projects. Why do they fail? Because changes are hard to make. Lets not make that mistake in our own pet project.

We left the code in `article.js` in this state:

```js
instagramFeed = document.getElementById('instagram-feed')

amountOfShortLinks = instagramShortLinks.length
for(number = 0; number < amountOfShortLinks ; number = number +1) {
  shortLink = instagramShortLinks[number]

  imageLocation = `https://instagram.com/p/${shortLink}/media/?size=m`

  image = document.createElement('img')
  image.src = imageLocation
  instagramFeed.appendChild(image)
}
```

#### Recognizing the abstraction

Our imageLocation in the code above has is too big to be useful for our hyperlink.

Actually, there are three parts of the URL, we just have to give them a good name:

The three parts named after their purpose:

* the `baseLocation = "https://instagram.com/p/"`
* the `shortLink` (that is set in our for loop)
* the `mediaSuffix = "/media/?size=m"`

Do you want a URL to the webpage? Use `baseLocation` and `shortLink`
Do you want a URL to the media? Use `baseLocation`, `shortLink` and `mediaSuffix`

In our `for` loop that means we have to add 2 more steps: Creating an `a`, with a `href` attribute
We have to nest the image inside that `a` element.

Most parts can stay the same. But since we split our URL to the image in 3 different parts, that should change as well

```js
instagramFeed = document.getElementById('instagram-feed')

// Lets name our URL parts:
baseLocation = "https://instagram.com/p/"
mediaSuffix = "/media/?size=m"

amountOfShortLinks = instagramShortLinks.length
for(number = 0; number < amountOfShortLinks ; number = number +1) {
  shortLink = instagramShortLinks[number]

  // create an image and set its source to instagram media
  image = document.createElement('img')
  image.src = baseLocation + shortLink + mediaSuffix

  // create a link and set the hyper reference to the instagram page.
  link = document.createElement('a')
  link.href = baseLocation + shortLink

  // append (or nest) the image in the link
  link.appendChild(image)

  // append the link to our instagram feed
  instagramFeed.appendChild(link)
}
```

---

## Exercise

**Goal of this exercise** is to have a piece of javascript that creates links with images, without you having to manually copy-paste links in your HTML.

Implement the abstractions we found to link your images to Instagram
