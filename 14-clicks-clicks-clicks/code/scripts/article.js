title = document.getElementById('title')
title.innerText = articleTitle

instagramFeed = document.getElementById('instagram-feed')

baseLocation = "https://instagram.com/p/"
mediaSuffix = "/media/?size=m"

amountOfShortLinks = instagramShortLinks.length

for(number = 0; number < amountOfShortLinks ; number = number +1) {
  shortLink = instagramShortLinks[number]

  image = document.createElement('img')
  image.src = baseLocation + shortLink + mediaSuffix

  link = document.createElement('a')
  link.href = baseLocation + shortLink
  link.appendChild(image)

  instagramFeed.appendChild(link)
}
