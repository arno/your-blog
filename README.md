# Hello

This is the repo backing the presentation at https://arno.gitlab.io/your-blog-presi/ (or view the code at https://gitlab.com/arno/your-blog-presi)

## Contributing

If there are any issues, feel free to [open an issue](https://gitlab.com/arno/your-blog/issues/new) or fork this repo, make fixes and [open a Merge Request](https://gitlab.com/arno/your-blog/merge_requests/new).

