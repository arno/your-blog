# Next steps

Where we go from here is a choice I leave to you.

![](../images/blue-pill-red-pill.jpeg)

4 (5?) different tracks!

* Write more articles, add images
* Update the style to make it pretty
* Add more interactions & make the code prettier
* Create an 'about-me' page with LinkedIn integration
* Or follow your own plan!

## After that: DEMO TIME

* Head over to [netlify.com/drop](https://app.netlify.com/drop), and host your weblog for everyone to see
